var animationAnchor = function () {
	$(document).on('click', 'header a[href^="#"], .welcome-area  a[href^="#"]', function (event) {
		event.preventDefault();

		$('html, body').animate({
			scrollTop: $($.attr(this, 'href')).offset().top
		}, 300);
	});
}

var mobileMenu = function () {
	$('.menu__icon').on('click', function (e) {
		e.preventDefault();
		$('.menu__list').slideToggle();
	})
}

$(function () {

	//Animation for links
	animationAnchor();

	//Mobile Menu
	mobileMenu();

});